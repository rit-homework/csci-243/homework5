CFLAGS=-Wall -Wextra -pedantic -std=c99

all: bst

bst: bst.o
	$(CC) -o bst bst.o

fix-formatting:
	clang-format -i *.c *.h

clean:
	rm bst{,.o}

.PHONY: clean fix-formatting
