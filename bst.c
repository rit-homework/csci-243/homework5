#include "bst.h"

#define _GNU_SOURCE
#include "bst.h"
#include <stdio.h>
#include <stdlib.h>

/// A mutable reference to a tree node. It's fields can be changed, but it can't
/// be reassigned.
typedef TreeNode *TreeNodeRef;

/// An immutable reference to a tree node. It's fields can be accessed, but it
/// can't (shouldn't) be reassigned or mutated.
typedef const TreeNode *ConstTreeNodeRef;

/// A mutable reference to a reference of a tree node. It can also be
/// re-assigned.
typedef TreeNodeRef *TreeNodeMutableRef;

/// A function which takes an integer and returns nothing. This is used by the
/// traversal helper functions.
typedef void (*Lambda)(int);

/// Allocates and populates a new TreeNode on the heap. This is basically a
/// constructor.
static TreeNodeRef new_node(int data, TreeNodeRef left, TreeNodeRef right) {
    TreeNode *node = malloc(sizeof(TreeNode));

    node->data = data;
    node->left = left;
    node->right = right;

    return node;
}

/// Given an insertion point (maybeRoot) and an item to insert (item), creates
/// appends the node to the BST.
static void insert(TreeNodeMutableRef maybeRoot, int item) {
    if (*maybeRoot == NULL) {
        // Base case, there is no element, create one.

        TreeNodeRef new_root = new_node(item, NULL, NULL);
        *maybeRoot = new_root;
        return;
    }

    TreeNodeRef root = *maybeRoot;
    if (item <= root->data) {
        // item <= root->data
        insert(&root->left, item);
    } else {
        // item > root->data
        insert(&root->right, item);
    }
}

void build_tree(TreeNode **root, const int elements[], const int count) {
    // Loop Over Elements
    for (int i = 0; i < count; i++) {
        int element = elements[i];

        // Insert Each Element
        insert(root, element);
    }
}

/// A helper function for the preorder traversal. The lambda is called for each
/// node as the tree is traversed.
static void traverse_preorder(ConstTreeNodeRef root, Lambda lambda) {
    if (root == NULL) {
        // Base case.
        return;
    }

    lambda(root->data);
    traverse_preorder(root->left, lambda);
    traverse_preorder(root->right, lambda);
}

/// A helper function for the postorder traversal. The lambda is called for each
/// node as the tree is traversed.
static void traverse_postorder(ConstTreeNodeRef root, Lambda lambda) {
    if (root == NULL) {
        // Base case.
        return;
    }

    traverse_postorder(root->left, lambda);
    traverse_postorder(root->right, lambda);
    lambda(root->data);
}

/// A helper function for the inorder traversal. The lambda is called for each
/// node as the tree is traversed.
static void traverse_inorder(ConstTreeNodeRef root, Lambda lambda) {
    if (root == NULL) {
        // Base case.
        return;
    }

    traverse_inorder(root->left, lambda);
    lambda(root->data);
    traverse_inorder(root->right, lambda);
}

/// A function which prints its argument. It satisfies the Lambda typedef.
static void print_number(int number) { printf("%d\n", number); }

void traverse(const TreeNode *root, const TraversalType type) {
    switch (type) {
    case PREORDER:
        traverse_preorder(root, print_number);
        break;

    case POSTORDER:
        traverse_postorder(root, print_number);
        break;

    case INORDER:
        traverse_inorder(root, print_number);
        break;

    default:
        // Invalid input arguments. There are no exceptions in C. There's no way
        // to report the error without violating the contract.
        break;
    }
}

/// Given a node, frees it's children, then itself.
static void cleanup_subtree(TreeNodeRef ref) {
    if (ref->left != NULL) {
        cleanup_subtree(ref->left);
    }

    if (ref->right != NULL) {
        cleanup_subtree(ref->right);
    }

    // Removed Both Subtrees, Cleanup Self
    free(ref);
}

void cleanup_tree(TreeNode *root) { cleanup_subtree(root); }

/// A container for additional information about traversal types.
typedef struct {
    /// The name of the traversal. This is printed before the traversal happens.
    const char *name;

    /// The type of the traversal. This is passed to the traverse function.
    TraversalType type;
} TraversalTypeInformation;

/// A list of traversals to do.
static TraversalTypeInformation traversals[] = {{
                                                    .name = "Preorder",
                                                    .type = PREORDER,
                                                },
                                                {
                                                    .name = "Inorder",
                                                    .type = INORDER,
                                                },
                                                {
                                                    .name = "Postorder",
                                                    .type = POSTORDER,
                                                }};

/// The total number of elements in the traversals array.
static int traversals_count = sizeof(traversals) / sizeof(traversals[0]);

/// The usage message. This is printed to standard error when not enough
/// arguments are provided.
static const char *USAGE = "Usage: bst #";

//// The message printed when a negative numbers is input.
static const char *INVALID_INPUT = "# must be greater than 0";

/// The format string for data input prompt.
static const char *PROMPT_FORMAT =
    "Enter %d integer values to place in tree:\n";

/// A helper function to prompt the user to enter data.
static void print_prompt(const int count) { printf(PROMPT_FORMAT, count); }

/// The main function. It parses input, builds a binary tree, traverses it and
/// cleans it up.
int main(const int argc, char *argv[]) {
    // Check Argument Count
    if (argc != 2) {
        fprintf(stderr, "%s\n", USAGE);
        return EXIT_FAILURE;
    }

    // Get Argument
    char *unparsed_argument = argv[1];

    // Parse Argument
    const int base10 = 10;
    int count = (int)strtol(unparsed_argument, NULL, base10);

    // Validate Argument
    if (count <= 0) {
        fprintf(stderr, "%s\n", INVALID_INPUT);
        return EXIT_FAILURE;
    }

    // Prompt User
    print_prompt(count);

    // Allocate Array of Elements
    int elements[count];

    // Populate Elements

    // A pointer to the buffer allocated by getline.
    char *line = NULL;

    // The size of the buffer allocated by getline.
    size_t len = 0;

    // Read count Lines
    for (int i = 0; i < count; i++) {
        // Read Each Line In (null terminated)
        ssize_t read = getline(&line, &len, stdin);
        if (read == -1) {
            // Something Went Wrong
            fprintf(stderr, "%s\n", "Failed to read line.");

            // Free Line
            free(line);

            return EXIT_FAILURE;
        }

        // Line Read, Parse Number
        int number = (int)strtol(line, NULL, base10);

        // There is no way to distinguish between an error an the number zero.
        // This assumes valid input.

        // Store Number
        elements[i] = number;
    }

    free(line);

    // Print Inputs
    printf("%s\n", "Input values:");
    for (int i = 0; i < count; i++) {
        printf("%d\n", elements[i]);
    }

    // Build Tree
    TreeNode *root = NULL;
    build_tree(&root, elements, count);

    // Print Traversals
    for (int i = 0; i < traversals_count; i++) {
        TraversalTypeInformation traversal = traversals[i];
        printf("%s:\n", traversal.name);
        traverse(root, traversal.type);
    }

    // Cleanup
    cleanup_tree(root);

    return EXIT_SUCCESS;
}
